
vim.cmd [[packadd plenary.nvim]]
vim.cmd [[packadd popup.nvim]]
vim.cmd [[packadd telescope.nvim]]

local Job = require('plenary.job')
local actions = require('telescope.actions')
local action_state = require('telescope.actions.state')
local finders = require('telescope.finders')
local pickers = require('telescope.pickers')
local previewers = require('telescope.previewers')
local sorters = require('telescope.sorters')

local function request_code_from_server(query, fileName, callback)
  Job:new({
    command = 'curl',
    args = {'-X', 'POST', '-H', 'Content-Type: application/json', '-d', vim.fn.json_encode({prompt = query, fileName = fileName}), 'http://localhost:7922/generate'},
    on_exit = function(j, return_val)
      local result = table.concat(j:result(), '\n')
      callback(result)
    end,
  }):start()
end

local function insert_code_into_buffer(code)
  local bufnr = vim.api.nvim_get_current_buf()
  local start_line = vim.fn.line('.')
  vim.api.nvim_buf_set_lines(bufnr, start_line, start_line, false, vim.split(code, '\n'))
end

local function prompt_for_code()
  local fileName = vim.fn.expand('%:t')
  local opts = {}
  pickers.new(opts, {
    prompt_title = 'Enter the code description',
    finder = finders.new_table({results = {}}),
    sorter = sorters.get_generic_fuzzy_sorter(),
    attach_mappings = function(prompt_bufnr, map)
      actions.select_default:replace(function()
        local selection = action_state.get_current_line()
        actions.close(prompt_bufnr)
        request_code_from_server(selection, fileName, function(result)
          insert_code_into_buffer(result)
        end)
      end)
      map('i', '<CR>', actions.select_default)
      return true
    end,
  }):find()
end

vim.api.nvim_set_keymap('n', '<leader>cc', [[<cmd>lua require('autonvim.plugin.telescope_codegen').prompt_for_code()<CR>]], { noremap = true, silent = true })

return {
  request_code_from_server = request_code_from_server,
  insert_code_into_buffer = insert_code_into_buffer,
  prompt_for_code = prompt_for_code,
}
