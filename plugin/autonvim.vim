
function! autonvim#RequestCodeFromServer(query, fileName)
  let command = 'curl -X POST -H "Content-Type: application/json" -d ' . shellescape('{"prompt": "' . a:query . '", "fileName": "' . a:fileName . '"}') . ' http://localhost:7922/generate'
  let result = system(command)
  call autonvim#InsertCodeIntoBuffer(result)
endfunction

function! autonvim#InsertCodeIntoBuffer(code)
  let lnum = line('.')
  call append(lnum, split(a:code, '\n'))
endfunction

function! autonvim#PromptForCode()
  let query = input('Enter the code description: ')
  let fileName = expand('%:t')
  call autonvim#RequestCodeFromServer(query, fileName)
endfunction

nnoremap <leader>cc :call autonvim#PromptForCode()<CR>

